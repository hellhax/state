﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace State.UI.ViewModels
{
    [DataContract]
    class SomethingStatefull
    {
        [DataMember]
        public int A
        {
            get;
            set;
        }

        [DataMember]
        public string B
        {
            get;
            set;
        }

        public decimal C
        {
            get;
            set;
        }
    }
}
