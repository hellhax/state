﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Caliburn.Micro;
using State.CaliburnExtensions;

namespace State.UI.ViewModels
{
    public class MainViewModel : Screen, IHaveState
    {
        private readonly INavigationService _navigationService;

        private string _someText;

        public string SomeText
        {
            get { return _someText; }
            set
            {
                if (_someText != value)
                {
                    _someText = value;
                    NotifyOfPropertyChange(() => SomeText);
                }
            }
        }

        public ICommand GoToSecondViewCommand
        {
            get;
            private set;
        }
        
        public ICommand GoToThirdViewCommand
        {
            get;
            private set;
        }

        public MainViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            GoToSecondViewCommand = new DelegateCommand(GoToSecondViewCommandExecuted);
            GoToThirdViewCommand = new DelegateCommand(GoToThirdViewCommandExecuted);
        }

        private void GoToSecondViewCommandExecuted()
        {
            _navigationService.NavigateToViewModel<SecondViewModel>();
        }

        private void GoToThirdViewCommandExecuted()
        {
            _navigationService.NavigateToViewModel<ThirdViewModel>();
        }

        public void SaveState(IState state)
        {
            state.Set("SomeText", SomeText);
        }

        public void LoadState(IState state)
        {
            if(state != null)
            {
                SomeText = state.Get<string>("SomeText");
            }
        }
    }
}
