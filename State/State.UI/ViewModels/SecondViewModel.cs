﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Caliburn.Micro;
using State.CaliburnExtensions;

namespace State.UI.ViewModels
{
    public class SecondViewModel : Screen, IHaveState
    {
        private readonly INavigationService _navigationService;
        private SomethingStatefull _somethingStatefull;

        private string _someOtherText;

        public string SomeOtherText
        {
            get { return _someOtherText; }
            set
            {
                if (_someOtherText != value)
                {
                    _someOtherText = value;
                    NotifyOfPropertyChange(() => SomeOtherText);
                }
            }
        }

        public ICommand GoBackCommand
        {
            get;
            private set;
        }
        
        public ICommand GoToThirdViewCommand
        {
            get;
            private set;
        }

        public SecondViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

            GoBackCommand = new DelegateCommand(GoBackCommandExecuted);
            GoToThirdViewCommand = new DelegateCommand(GoToThirdViewCommandExecuted);

            _somethingStatefull = new SomethingStatefull { A = 23, B = DateTime.Now.ToString(), C = 23.456m };
        }

        private void GoBackCommandExecuted()
        {
            _navigationService.GoBack();
        }

        private void GoToThirdViewCommandExecuted()
        {
            _navigationService.NavigateToViewModel<ThirdViewModel>();
        }

        public void SaveState(IState state)
        {
            state.Set("SomeOtherText", SomeOtherText);
            state.Set("Statefull", _somethingStatefull);
        }

        public void LoadState(IState state)
        {
            if(state != null)
            {
                SomeOtherText = state.Get<string>("SomeOtherText");
                _somethingStatefull = state.Get<SomethingStatefull>("Statefull");
            }
        }
    }
}
