﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Caliburn.Micro;
using State.CaliburnExtensions;

namespace State.UI.ViewModels
{
    public class ThirdViewModel : Screen, IHaveState
    {
        private readonly INavigationService _navigationService;

        private string _someDifferentText;

        public string SomeDifferentText
        {
            get { return _someDifferentText; }
            set
            {
                if (_someDifferentText != value)
                {
                    _someDifferentText = value;
                    NotifyOfPropertyChange(() => SomeDifferentText);
                }
            }
        }

        public ICommand GoBackCommand
        {
            get;
            private set;
        }
        
        public ThirdViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            GoBackCommand = new DelegateCommand(GoBackCommandExecuted);
        }

        private void GoBackCommandExecuted()
        {
            _navigationService.GoBack();
        }

        public void SaveState(IState state)
        {
            state.Set("SomeDifferentText", SomeDifferentText);
        }

        public void LoadState(IState state)
        {
            if (state != null)
            {
                SomeDifferentText = state.Get<string>("SomeDifferentText");
            }
        }
    }
}
