﻿using Autofac;
using Caliburn.Micro;
using State.CaliburnExtensions;
using State.UI.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using State.UI.ViewModels;

namespace State.UI
{
    sealed partial class App : CaliburnApplication
    {
        private ContainerBuilder _containerBuilder;
        private IContainer _container;
        private bool _suspended;

        private StateAwareFrameAdapter _navigationService;

        public App()
        {
            InitializeComponent();
            Suspending += OnSuspending;
        }

        protected override void Configure()
        {
            _containerBuilder = new ContainerBuilder();
            _containerBuilder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();
            _containerBuilder.RegisterType<SharingService>().As<ISharingService>().SingleInstance();
            _containerBuilder.RegisterType<SettingsWindowManager>().As<ISettingsWindowManager>().SingleInstance();
            _containerBuilder.RegisterType<SettingsService>().As<ISettingsService>().SingleInstance();
            _containerBuilder.RegisterType<MainViewModel>();
            _containerBuilder.RegisterType<SecondViewModel>();
            _containerBuilder.RegisterType<ThirdViewModel>();

            PrepareViewFirst();

            _container = _containerBuilder.Build();
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.Resolve(service);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            throw new NotImplementedException("GetAllInstances is not implemented");
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            _navigationService = new StateAwareFrameAdapter(rootFrame);
            _navigationService.RegisterStateKnownType(typeof(SomethingStatefull));

            _containerBuilder.RegisterInstance((FrameAdapter)_navigationService).As<INavigationService>().SingleInstance();
        }

        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            Initialize();
            
            bool resumed = false;
            if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
            {
                await _navigationService.RestoreStateAsync();
                resumed = _navigationService.ResumeState();
            }

            if (!resumed)
            {
                DisplayRootView<MainView>();
            }
        }

        protected override async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            if(!_suspended)
            {
                var deferral = e.SuspendingOperation.GetDeferral();
                await _navigationService.SuspendStateAsync();
                _navigationService.SuspendState();
                _suspended = true;
                deferral.Complete();
            }
        }

        protected override void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            throw new InvalidOperationException("Unhandled exception occured", e.Exception);
        }
    }
}
