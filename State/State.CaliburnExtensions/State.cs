﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace State.CaliburnExtensions
{
    [DataContract]
    class State : IState
    {
        [DataMember]
        private readonly Dictionary<string, object> _stateDictionary;
        private readonly HashSet<Type> _knownTypes;

        internal IEnumerable<Type> KnownTypes
        {
            get { return _knownTypes; }
        }

        public State()
        {
            _stateDictionary = new Dictionary<string, object>();
            _knownTypes = new HashSet<Type>();
        }

        public object Get(string key)
        {
            object value;
            _stateDictionary.TryGetValue(key, out value);
            return value;
        }

        public T Get<T>(string key)
        {
            return (T) Get(key);
        }

        public void Set(string key, object value)
        {
            _stateDictionary[key] = value;
        }

        public void Set<T>(string key, T value)
        {
            Set(key, (object)value);
        }

        public void RegisterKnownType(Type type)
        {
            _knownTypes.Add(type);
        }
    }
}
