﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace State.CaliburnExtensions
{
    public class DelegateCommand : ICommand
    {
        private readonly Func<bool> canExcecuteChangedDelegate;
        private readonly Action executeDelegate;

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter)
        {
            return this.canExcecuteChangedDelegate();
        }

        public void Execute(object parameter)
        {
            this.executeDelegate();
        }

        public DelegateCommand(Func<bool> canExcecuteChangedDelegate, Action executeDelegate)
        {
            this.canExcecuteChangedDelegate = canExcecuteChangedDelegate;
            this.executeDelegate = executeDelegate;
        }

        public DelegateCommand(Action executeDelegate)
        {
            this.canExcecuteChangedDelegate = () => true;
            this.executeDelegate = executeDelegate;
        }

        public void NotifyCanExecuteChanged()
        {
            this.CanExecuteChanged(this, EventArgs.Empty);
        }
    }

    public class DelegateCommand<T> : ICommand
    {
        private readonly Func<bool> canExcecuteChangedDelegate;
        private readonly Action<T> executeDelegate;

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter)
        {
            return this.canExcecuteChangedDelegate();
        }

        public void Execute(object parameter)
        {
            this.executeDelegate((T)parameter);
        }

        public DelegateCommand(Func<bool> canExcecuteChangedDelegate, Action<T> executeDelegate)
        {
            this.canExcecuteChangedDelegate = canExcecuteChangedDelegate;
            this.executeDelegate = executeDelegate;
        }

        public DelegateCommand(Action<T> executeDelegate)
        {
            this.canExcecuteChangedDelegate = () => true;
            this.executeDelegate = executeDelegate;
        }

        public void NotifyCanExecuteChanged()
        {
            this.CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
