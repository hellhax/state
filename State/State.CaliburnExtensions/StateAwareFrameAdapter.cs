﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Caliburn.Micro;

namespace State.CaliburnExtensions
{
    public class StateAwareFrameAdapter : FrameAdapter
    {
        private const string SessionStateFilename = "SessionState.xml";

        private readonly Frame _frame;
        private readonly HashSet<Type> _knownTypes;
        private Dictionary<int, object> _pagesStates;

        public StateAwareFrameAdapter(Frame frame, bool treatViewAsLoaded = false) : base(frame, treatViewAsLoaded)
        {
            _frame = frame;
            _knownTypes = new HashSet<Type>(new[] { typeof(global::State.CaliburnExtensions.State) });
            _pagesStates = new Dictionary<int, object>();
        }

        public void RegisterStateKnownType(Type type)
        {
            _knownTypes.Add(type);
        }

        public async Task SuspendStateAsync()
        {
            SaveState();
            await PersistStateAsync();
        }

        private async Task PersistStateAsync()
        {
            try
            {
                MemoryStream sessionData = new MemoryStream();
                DataContractSerializer serializer = new DataContractSerializer(typeof(Dictionary<int, object>), _knownTypes);
                serializer.WriteObject(sessionData, _pagesStates);

                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync(SessionStateFilename, CreationCollisionOption.ReplaceExisting);
                using (Stream fileStream = await file.OpenStreamForWriteAsync())
                {
                    sessionData.Seek(0, SeekOrigin.Begin);
                    await sessionData.CopyToAsync(fileStream);
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("An error occured while persisting view model states", ex);
            }
        }

        public async Task RestoreStateAsync()
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(SessionStateFilename);
                using (IInputStream inStream = await file.OpenSequentialReadAsync())
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof (Dictionary<int, object>), _knownTypes);
                    _pagesStates = (Dictionary<int, object>) serializer.ReadObject(inStream.AsStreamForRead());
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("An error occured while restoring view model states", ex);
            }
        }

        protected override void TryInjectParameters(object viewModel, object parameter)
        {
            base.TryInjectParameters(viewModel, parameter);
            
            IHaveState haveState = viewModel as IHaveState;
            if(haveState != null)
            {
                IState state = GetCurrentState();
                haveState.LoadState(state);
            }
        }

        protected override void OnNavigating(object sender, NavigatingCancelEventArgs e)
        {
            base.OnNavigating(sender, e);
            SaveState();
        }

        protected override void OnNavigated(object sender, NavigationEventArgs e)
        {
            base.OnNavigated(sender, e);

            int i = _frame.BackStackDepth + 1;
            while (_pagesStates.Remove(i))
            {
                i++;
            }
        }

        private void SaveState()
        {
            FrameworkElement view = _frame.Content as FrameworkElement;
            if (view != null && view.DataContext != null)
            {
                IHaveState haveState = view.DataContext as IHaveState;
                if (haveState != null)
                {
                    IState state = GetOrCreateCurrentState();
                    haveState.SaveState(state);
                }
            }
        }

        private global::State.CaliburnExtensions.State GetOrCreateCurrentState()
        {
            global::State.CaliburnExtensions.State currentState = GetCurrentState();
            if(currentState == null)
            {
                currentState = new global::State.CaliburnExtensions.State();
                _pagesStates.Add(_frame.BackStackDepth, currentState);
            }
            return currentState;
        }
        
        private global::State.CaliburnExtensions.State GetCurrentState()
        {
            object currentState;
            _pagesStates.TryGetValue(_frame.BackStackDepth, out currentState);
            return (global::State.CaliburnExtensions.State)currentState;
        }
    }
}