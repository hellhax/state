﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State.CaliburnExtensions
{
    public interface IHaveState
    {
        void SaveState(IState state);
        void LoadState(IState state);
    }
}
