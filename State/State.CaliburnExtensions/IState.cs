﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State.CaliburnExtensions
{
    public interface IState
    {
        object Get(string key);
        T Get<T>(string key);

        void Set(string key, object value);
        void Set<T>(string key, T value);

        void RegisterKnownType(Type type);
    }
}
